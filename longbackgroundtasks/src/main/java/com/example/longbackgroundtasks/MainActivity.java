package com.example.longbackgroundtasks;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

/**
 * 后台执行定时任务
 * 两种实现形式，一种是利用Java API的Timer类，缺点在于不适合长期后台应用；另一种是Android的alarm机制，可以唤醒CPU
 * <p>
 * TODO:alarm机制利用了AlarmManager类实现功能，和通知管理器类似，调用Context的getSystemService方法获取实例
 * <p>
 * 最终实现的效果，TODO:该模组能够在后台每间隔4s打印一次系统当前时间。
 *
 * @author MAC
 */
public class MainActivity extends AppCompatActivity {
    private View view;
    private WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.web_view1);
        //调用了 setJavaScriptEnabled()方法来让 WebView 支持 JavaScript 脚本
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //根据传入的参数加载新的网页
                webView.loadUrl(url);
                //标识当前的webview可以处理打开新的网页 不需要借助系统的浏览器
                return true;
            }
        });
        webView.loadUrl("https://www.baidu.com/");

        Intent intent = new Intent(this, LongRunningService.class);
        startService(intent);
    }
}