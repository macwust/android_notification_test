package com.example.servicetest;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * intentService就是用来防止程序员开启线程或者忘记stopSelf，
 * 这里首先是要提供一个无参的构造函数，并且必须在其内部调用父类的有参构造函数
 * 另外根据 IntentService 的特性：
 * TODO:这个服务在运行结束后应该是会自动停止的，所以我们又重写了 onDestroy()方法，在这里也打印了一行日志，以证实服务是不是停止掉了
 *
 * @author MAC
 */
public class MyIntentService extends IntentService {
    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("MyIntentService", "Thread id is " + Thread.currentThread().getId());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyIntentService", "onDestroy executed");
    }
}
