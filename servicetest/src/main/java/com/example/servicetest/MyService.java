package com.example.servicetest;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

/**
 * 创建了前台服务，调用startForeground方法，接受两个参数，一个是通知id，一个是构建通知对象。
 *
 * @author MAC
 */
public class MyService extends Service {
    private DownloadBinder mBinder = new DownloadBinder();
    private Context mContext;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MyService", "onBind success");
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        /*
        首先需要一个NotificationManager来对通知进行管理
        调用Context的getSystemService()方法获取到。
        getSystemService()方法接受的一个字符串参数用于确定系统的的哪一个服务。
         */
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        /*
        调用NotificationChannel创建通知渠道实例
        设置属性
         */
        //通知渠道的id
        String id = "channel_01";
        //用户可以看到渠道id
        CharSequence name = getString(R.string.channel_name);
        //用户可以看到的通知描述
        String description = getString(R.string.channel_description);
        //创建NotificationChannel实例
        NotificationChannel notificationChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
        }
        //设置通知渠道的属性
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel.setDescription(description);
        }
        //设置出现通知的闪光灯
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel.enableLights(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel.setLightColor(Color.RED);
        }
        //设置通知出现时的震动
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel.enableVibration(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 100});
        }
        //在notificationManager中创建通知渠道
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("ServiceTest")
                .setContentText("This is service test at 0822")
                //实现点击跳转通知跳转
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background))
                .setWhen(System.currentTimeMillis())
                //实现点击跳转通知后关闭
                .setAutoCancel(true)
                .build();
        /*
        调用NotificationManager的notify()方法将通知显示出来
        传入的第一个参数是通知的id
        传入的第二个参数是notification对象
         */
        notificationManager.notify(1, notification);
        Log.d("MyService", "onCreate executed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyService", "onStartCommand executed");
        new Thread(() -> {

        }).start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyService", "onDestroy executed");
    }

    class DownloadBinder extends Binder {
        public void startDownload() {
            new Thread(() -> {

            }).start();
            Log.d("MyService", "startDownload executed");
        }

        public int getProgress() {
            Log.d("MyService", "getProgress executed");
            return 0;
        }
    }
}
