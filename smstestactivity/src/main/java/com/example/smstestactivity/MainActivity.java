package com.example.smstestactivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author MAC
 */
public class MainActivity extends Activity {
    private TextView sender;
    private TextView content;
    private EditText to;
    private EditText msgInput;
    private Button send;
    private IntentFilter receiveFilter;
    private MessageReceiver messageReceiver;
    private IntentFilter sendFilter;
    private SendStatusReceiver sendStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        receiveFilter = new IntentFilter();
        receiveFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        receiveFilter.setPriority(100);
        messageReceiver = new MessageReceiver();
        registerReceiver(messageReceiver, receiveFilter);
        sendFilter = new IntentFilter();
        sendFilter.addAction("SENT_SMS_ACTION");
        sendStatusReceiver = new SendStatusReceiver();
        registerReceiver(sendStatusReceiver, sendFilter);
        send.setOnClickListener(v -> {
            SmsManager smsManager = SmsManager.getDefault();
            Intent sendIntent = new Intent("SENT_SMS_ACTION");
            PendingIntent pi = PendingIntent.getBroadcast(MainActivity.this, 0, sendIntent, 0);
            smsManager.sendTextMessage(to.getText().toString(), null, msgInput.getText().toString(), pi, null);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(messageReceiver);
        unregisterReceiver(sendStatusReceiver);
    }

    public void initView() {
        sender = findViewById(R.id.sender);
        content = findViewById(R.id.content);
        to = findViewById(R.id.to);
        msgInput = findViewById(R.id.msg_input);
        send = findViewById(R.id.send);
    }

    private class MessageReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage[] messages = new SmsMessage[pdus.length];
            for (int i = 0; i < messages.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            String address = messages[0].getOriginatingAddress();
            StringBuilder fullMessage = new StringBuilder();
            for (SmsMessage message : messages) {
                fullMessage.append(message.getMessageBody());
            }
            sender.setText(address);
            content.setText(fullMessage);
            abortBroadcast();
        }
    }

    private class SendStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getResultCode() == RESULT_OK) {
                Toast.makeText(context, "send successful!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "send failed! Please check your sms network", Toast.LENGTH_SHORT).show();
            }
        }
    }
}