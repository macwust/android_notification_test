package com.example.notificationtest;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //private ActivityCheckNotifyBinding mBinding;

    private Button sendNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sendNotice = findViewById(R.id.send_notice);
        sendNotice.setOnClickListener(this);
    }

    /**
     * TODO:Lint是一个静态检查器，它围绕Android项目的正确性、安全性、性能、可用性以及可访问性进行分析
     *
     * @param v
     */
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_notice:
                Toast.makeText(this, "已发送系统通知，请及时查看", Toast.LENGTH_SHORT).show();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                Notification.Builder builder1 = new Notification.Builder(MainActivity.this);
                //设置图标
                builder1.setSmallIcon(R.drawable.ic_launcher_background);
                builder1.setTicker("显示第1个通知");
                //设置标题
                builder1.setContentTitle("通知");
                //消息内容
                builder1.setContentText("点击查看详细内容");
                //发送时间
                builder1.setWhen(System.currentTimeMillis());
                //设置默认的提示音，振动方式，灯光
                builder1.setDefaults(Notification.DEFAULT_ALL);
                //打开程序后图标消失
                builder1.setAutoCancel(true);
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);
                builder1.setContentIntent(pendingIntent);
                Notification notification1 = builder1.build();
                // 通过通知管理器发送通知
                notificationManager.notify(1, notification1);
            default:
                break;
        }
    }

//    private void checkNotificationSetting() {
//        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(this);
//        // areNotificationsEnabled方法的有效性官方只最低支持到API 19，低于19的仍可调用此方法不过只会返回true，即默认为用户已经开启了通知。
//        boolean isOpened = managerCompat.areNotificationsEnabled();
//
//        if (isOpened) {
//
//        }
//    }
}